<?php
  session_start();
  if (empty($_SESSION['username'])) {
    header("Location: login.php");
    die("You need to be logged in to access this page.");
  }

 ?>
